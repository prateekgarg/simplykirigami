set(simplykirigami_SRCS
    main.cpp
    )

qt5_add_resources(RESOURCES resources.qrc)

add_executable(simplykirigami ${simplykirigami_SRCS} ${RESOURCES})
target_link_libraries(simplykirigami Qt5::Core Qt5::Qml Qt5::Quick Qt5::Svg KF5::I18n)

if(ANDROID)
    kirigami_package_breeze_icons(ICONS
        view-list-icons go-home go-previous go-next bookmarks folder
    )
endif()

install(TARGETS simplykirigami ${KF5_INSTALL_TARGETS_DEFAULT_ARGS})
